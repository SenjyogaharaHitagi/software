
import requests
import json
import pandas as pd
import pymysql
header = {
    'User-Agent':'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Mobile Safari/537.36',
    'Cookie': 'UM_distinctid=17a1389722162c-07c97346996a09-f7d123e-144000-17a1389722274b; CNZZDATA1254581267=215249020-1623820908-%7C1623907311',
    'Referer': 'https://www.cingta.com/school/ser'
}

url = "https://www.cingta.com/school/api/name_uni_list/"

resp = requests.post(url=url,headers=header).json()
data = resp['data']

colleges = data['list']

pd.DataFrame(colleges).to_excel('教育部高校名单.xls')