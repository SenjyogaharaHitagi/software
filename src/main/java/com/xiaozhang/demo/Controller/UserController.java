package com.xiaozhang.demo.Controller;

import com.xiaozhang.demo.Dto.JsonResponse;
import com.xiaozhang.demo.Dto.School;
import com.xiaozhang.demo.Mapper.SchoolDao;
import com.xiaozhang.demo.Dto.SchoolExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("school")
public class UserController {

    @Autowired
    private SchoolDao schoolDao;

    @PostMapping("getData")
    @CrossOrigin
    public JsonResponse<List> List(@RequestBody School school){
        JsonResponse<List> json = new JsonResponse<>();
        String type = school.getType();
        String schoolName = school.getSchoolName();
        HashMap<String, String> typeMap = new HashMap<>();
        typeMap.put("","");
        typeMap.put("1","%一流大学%");
        typeMap.put("2","%一流学科%");
        typeMap.put("3","%985%");
        typeMap.put("4","%211%");
        SchoolExample schoolExample = new SchoolExample();
        SchoolExample.Criteria criteria =schoolExample.createCriteria();
        if(type!="")
            criteria.andTypeLike(typeMap.get(type));
        if(schoolName!="")
            criteria.andSchoolNameLike("%"+schoolName+"%");
        List<School> schools = schoolDao.selectByExample(schoolExample);
        json.setResult(schools);
        return json;
    }
}
