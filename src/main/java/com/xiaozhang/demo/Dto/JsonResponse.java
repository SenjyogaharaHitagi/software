package com.xiaozhang.demo.Dto;

import java.io.Serializable;

public class JsonResponse<T> implements Serializable {
    private static final long serialVersionUID = 7705290087975145222L;
    private boolean success;
    private T result;
    private String errorCode;
    private String errorMsg;

    public JsonResponse() {
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getResult() {
        return this.result;
    }

    public void setResult(T result) {
        this.success = Boolean.TRUE;
        this.result = result;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return this.errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}